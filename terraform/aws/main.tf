# PROVIDER
provider "aws" {
  region = var.region
}

resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = file(var.aws_public_key_ssh_path)
}

resource "aws_key_pair" "deployer" {
	key_name = "deployer-key"
	public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDipkKnUac2GybYkFvLr5dClV9he7Ueloi3uBRHOWAyRB/hkrRinmv1GiI1g6dPVQq9dI8QtUBo4yrqrwhZq+gIGCF01wi5sKwumxdOJaIIAuD4Gb2zaFBkGh6LSl1bMd4mCnQPvEog7zREA1VepZ4uiU6FzFqBweoOBKqejSdwHLO/Sj0RGGHODE85/gA+P4tB7pO7480PEZ26J0FKxHY8IpPdjnnJy18LzdlmZc/Jusjfg6qvWOmPKdztztEEc2a4UMBRpyq98avr0tiQqikSXI/R09Ap2qxT4v5CsWgFSKWWo6VnmFrnS8mdiovjd0Z4CEeT6Jmhh9vf6MRHnGk7 martin@martiiUbuntu"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}

